module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'], 
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors:{
        red1:'#FF4C00',
      },
      screens:{
        cmd:'450px'
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
