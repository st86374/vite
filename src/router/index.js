import { createRouter, createWebHistory } from "vue-router";
const routes = [
    {
        path: "/",
        name: "Home",
        component: () => import("/src/views/index.vue"),
      },
      {
        path: "/map",
        name: "Map",
        component: () => import("/src/views/map.vue"),
      },
]
export default createRouter({
    history: createWebHistory(),
    routes,
  });
  