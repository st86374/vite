import { createStore } from "vuex";

const store = createStore({
  state() {
    return {
      count: 0,
    };
  },
  mutations: {
    countAddMutations(state) {
      state.count++;
    },
  },
  actions:{
    countAddActions(context) {
        context.commit("countAddMutations");
      },
  },
  getters: {
    count(state) {
      return state.count;
    },
  },
});

export default store;
